package internal

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
)

type Command interface {
	Command() (fs *flag.FlagSet)
	Execute(ctx context.Context) error
}

type Commands []Command

func (c Commands) Usage() {
	for idx, cmd := range c {
		fs := cmd.Command()

		if idx == 0 {
			fmt.Fprintln(fs.Output(), "USAGE:")
		}

		fmt.Fprintln(fs.Output())
		fs.Usage()
	}

	os.Exit(1)
}

func Run(ctx context.Context, cmds Commands) {
	if len(os.Args) < 2 {
		cmds.Usage()
	}

	for _, cmd := range cmds {
		fs := cmd.Command()
		if os.Args[1] == fs.Name() {
			fs.Parse(os.Args[2:])

			if err := cmd.Execute(ctx); err != nil {
				if errors.Is(err, flag.ErrHelp) {
					fs.Usage()
					os.Exit(1)
				}

				fmt.Println(err)
				os.Exit(1)
			}
			return
		}
	}

	cmds.Usage()
}
