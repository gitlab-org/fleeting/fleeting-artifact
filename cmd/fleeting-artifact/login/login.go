package login

import (
	"context"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"

	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/installer"
)

type loginCmd struct {
	fs *flag.FlagSet

	username string
	password string
	stdin    bool
}

func New() *loginCmd {
	c := &loginCmd{}
	c.fs = flag.NewFlagSet("login", flag.ExitOnError)

	c.fs.StringVar(&c.username, "username", "", "Username")
	c.fs.StringVar(&c.password, "password", "", "Password")
	c.fs.BoolVar(&c.stdin, "password-stdin", false, "Take the password from stdin")

	c.fs.Usage = func() {
		fmt.Fprintln(c.fs.Output(), c.fs.Name(), "[OPTIONS] [SERVER]")
		fmt.Fprintf(c.fs.Output(), "\n  Login to container registry\n")
		c.fs.PrintDefaults()
	}

	return c
}

func (cmd *loginCmd) Command() *flag.FlagSet {
	return cmd.fs
}

func (cmd *loginCmd) Execute(ctx context.Context) error {
	if cmd.stdin {
		pass, err := io.ReadAll(os.Stdin)
		if err != nil {
			return fmt.Errorf("reading password from stdin: %w", err)
		}
		cmd.password = strings.TrimSuffix(strings.TrimSuffix(string(pass), "\n"), "\r")
	}

	via, err := installer.Login(cmd.fs.Arg(0), cmd.username, cmd.password)
	if err != nil {
		return fmt.Errorf("login: %w", err)
	}

	fmt.Println("logged in via", via)
	return nil
}
