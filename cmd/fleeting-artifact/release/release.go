package release

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/releaser"
)

type releaseCmd struct {
	fs *flag.FlagSet

	dir string
}

func New() *releaseCmd {
	c := &releaseCmd{}
	c.fs = flag.NewFlagSet("release", flag.ExitOnError)

	c.fs.StringVar(&c.dir, "dir", "./dist", "distribution directory")

	c.fs.Usage = func() {
		fmt.Fprintln(c.fs.Output(), c.fs.Name(), "[OPTIONS] NAME:VERSION")
		fmt.Fprintf(c.fs.Output(), "\n  Uploads plugin artifacts and tags versions.\n\n")
		c.fs.PrintDefaults()
	}

	return c
}

func (cmd *releaseCmd) Command() *flag.FlagSet {
	return cmd.fs
}

func (cmd *releaseCmd) Execute(ctx context.Context) error {
	if len(cmd.fs.Args()) < 1 {
		return flag.ErrHelp
	}

	digestUrl, err := releaser.Release(ctx, cmd.fs.Arg(0), releaser.WithDirectory(cmd.dir))
	if err != nil {
		return err
	}

	fmt.Println(digestUrl)

	return nil
}
