package install

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/installer"
)

type installCmd struct {
	fs *flag.FlagSet
}

func New() *installCmd {
	c := &installCmd{}
	c.fs = flag.NewFlagSet("install", flag.ExitOnError)

	c.fs.Usage = func() {
		fmt.Fprintln(c.fs.Output(), c.fs.Name(), "NAME:VERSION_CONSTRAINT")
		fmt.Fprintf(c.fs.Output(), "\n  Downloads and installs the fleeting plugin locally.\n")
		c.fs.PrintDefaults()
	}

	return c
}

func (cmd *installCmd) Command() *flag.FlagSet {
	return cmd.fs
}

func (cmd *installCmd) Execute(ctx context.Context) error {
	if len(cmd.fs.Args()) < 1 {
		return flag.ErrHelp
	}

	return installer.Install(ctx, cmd.fs.Arg(0))
}
