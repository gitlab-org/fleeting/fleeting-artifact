package main

import (
	"context"
	"os"
	"os/signal"

	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/cmd/fleeting-artifact/install"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/cmd/fleeting-artifact/list"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/cmd/fleeting-artifact/login"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/cmd/fleeting-artifact/lookpath"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/cmd/fleeting-artifact/release"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/cmd/fleeting-artifact/updatealiases"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/cmd/internal"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	internal.Run(ctx, internal.Commands{
		login.New(),
		lookpath.New(),
		release.New(),
		install.New(),
		list.New(),
		updatealiases.New(),
	})
}
