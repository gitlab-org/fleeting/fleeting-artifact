# fleeting-artifact

`fleeting-artifact` is a tool and library for distributing `fleeting` plugins
using an OCI registry.

When a `fleeting` plugin is uploaded to an OCI registry, it is packaged in such
a way that is it both a valid Docker image (that can be used by Docker, Podman,
Kubernetes etc.) and a generic package that can be installed on most systems
without a container runtime (leveraging platform negotiation to select the
correct package for your system).

## Plugin versioning

Plugin versions are expected to follow the Semantic Version 2 specification,
but build metadata is not supported.

`fleeting-artifact` manages aliased versions by retagging `latest`,
major and minor version tags as necessary. For example, if you push the
version `1.2.3` and this is the most recent version pushed to the repository,
then this will also have the tag aliases `latest`, `1` and `1.2`.

## Releasing a plugin

Plugins are typically statically compiled Go binaries. The plugin should be
compiled for each supported system and placed in a directory following the
pattern: `<os>/<arch>[variant]`. The binary name should be `plugin`
(or `plugin.exe` on Windows).

For example, a plugin compiled for `linux` targeting `armv6` would be found at:
`linux/armv6/plugin`.

These os/arch combinations are supported:

- `linux/386`
- `linux/amd64`
- `linux/armv5`
- `linux/armv6`
- `linux/armv7`
- `linux/arm64`
- `linux/mips64le`
- `linux/ppc64le`
- `linux/riscv64`
- `linux/s390x`
- `freebsd/386`
- `freebsd/amd64`
- `freebsd/armv5`
- `freebsd/armv6`
- `freebsd/armv7`
- `freebsd/arm64`
- `darwin/amd64`
- `darwin/arm64`
- `windows/386`
- `windows/amd64`
- `zos/s390x`

The following command will package all binaries found under the directory
`dist` (this is also the default if none is provided) and push the artifacts to
`index.docker.io/username/my-fleeting-plugin` for the version `1.2.0`:

```shell
fleeting-artifact release -dir dist index.docker.io/username/my-fleeting-plugin:1.2.0
```

## Installing a plugin

Installing a plugin might be supported by the application that supports `fleeting`,
by using `fleeting-artifact` as a library. However, for convenience, the
`fleeting-artifact` cli tool also offers this capability.

This command can be used to install a plugin for your system:

```shell
fleeting-artifact install index.docker.io/username/my-fleeting-plugin:1.2.0
```

`1.2.0` can also be replaced with a version constraint/alias, such as `1`,
`1.2` or `latest`.

Plugins are installed to `~/.config/fleeting/plugins` on unix systems, and
`%APPDATA%/fleeting/plugins` on Windows. This can be overridden with the
environment variable `FLEETING_PLUGIN_PATH`.

A plugin can also be installed by using the plugin artifact as a Docker image,
and bootstrap installing it into the well-known fleeting plugin path.

For example, with Docker, this can be done by running the following:

```shell
docker run --rm -v ~/.config/fleeting/plugins:/plugins index.docker.io/username/my-fleeting-plugin:1.2 bootstrap index.docker.io/username/my-fleeting-plugin
```

This command mounts the well-known fleeting path to `/plugins`, runs the plugin
binary with the `bootstrap` sub-command which installs itself to the correct
directory under the repository name `index.docker.io/username/my-fleeting-plugin`.

Installation in this manner allows applications that use fleeting and run on
platforms such as Kubernetes to use the underlying container runtime as a
plugin cache, avoiding the need for plugin installation for each container run.

## Using a plugin

Applications that consume `fleeting` plugins (such as `gitlab-runner`) can load
a plugin by referencing the repository and version constraint.

Loading in this way will only use already installed plugins. For example, using
the version constraint `latest` will only ever use the latest installed plugin
for your system. To update this, the latest artifact would need to be pulled
again.
