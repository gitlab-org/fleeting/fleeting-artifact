package defname

import (
	"path"
	"strings"

	"github.com/google/go-containerregistry/pkg/name"
)

const (
	DefaultRegistry  = "registry.gitlab.com"
	DefaultNamespace = "gitlab-org/fleeting/plugins"
)

func ParseReference(addr string, opts ...name.Option) (name.Reference, error) {
	parts := strings.Split(addr, "/")
	switch {
	case len(parts) == 1:
		addr = path.Join(DefaultRegistry, DefaultNamespace, addr)
	case len(parts) > 1 && !strings.ContainsRune(parts[0], '.'):
		addr = path.Join(DefaultRegistry, addr)
	}

	ref, err := name.ParseReference(addr, opts...)
	if err != nil {
		return ref, err
	}

	return ref, nil
}
