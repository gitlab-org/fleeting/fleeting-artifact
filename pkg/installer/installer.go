package installer

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/client"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/internal/defname"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/version"
	"golang.org/x/mod/module"
)

var ErrPluginNotFound = errors.New("plugin not found")

func InstallDir() string {
	name := os.Getenv("FLEETING_PLUGIN_PATH")
	if name != "" {
		return name
	}

	if runtime.GOOS == "windows" {
		name = filepath.Join(os.Getenv("APPDATA"))
	} else {
		home, _ := os.UserHomeDir()
		name = filepath.Join(home, ".config")
	}

	return filepath.Join(name, "fleeting", "plugins")
}

func Install(ctx context.Context, addr string, opts ...Option) error {
	// backwards compatibility: skip install if plugin is a binary with the prefix 'fleeting-plugin-' found within $PATH
	if _, ok := isLocalPathBinary(addr); ok {
		return nil
	}

	var options options
	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return err
		}
	}

	if options.dir == "" {
		options.dir = InstallDir()
	}

	client, err := client.New()
	if err != nil {
		return fmt.Errorf("creating client: %w", err)
	}

	return client.Pull(ctx, addr, options.dir)
}

func LookPath(addr string, pluginsDir string) (string, error) {
	if pluginsDir == "" {
		pluginsDir = InstallDir()
	}

	// backwards compatibility: return if addr is a binary with the prefix 'fleeting-plugin-' found within $PATH
	if p, ok := isLocalPathBinary(addr); ok {
		return p, nil
	}

	ref, err := defname.ParseReference(addr)
	if err != nil {
		return "", fmt.Errorf("parsing reference: %w", err)
	}

	constraint, err := version.NewConstraint(ref.Identifier())
	if err != nil {
		return "", fmt.Errorf("parsing constraint: %w", err)
	}

	pluginDir, err := module.EscapePath(ref.Context().Name())
	if err != nil {
		return "", fmt.Errorf("escaping plugin path: %w", err)
	}
	pluginDir = filepath.Join(pluginsDir, pluginDir)

	var versions []version.Version
	if err := filepath.WalkDir(pluginDir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if !d.IsDir() {
			return nil
		}

		v, err := module.UnescapeVersion(d.Name())
		if err != nil {
			return nil // ignore error
		}

		ver, err := version.New(v)
		if err != nil {
			return nil // ignore invalid versions
		}

		versions = append(versions, ver)

		return nil
	}); err != nil {
		return "", fmt.Errorf("%w: %w", ErrPluginNotFound, err)
	}

	versions = constraint.Match(versions)
	if len(versions) == 0 {
		return "", fmt.Errorf("%w: %s", ErrPluginNotFound, pluginDir)
	}

	selected := versions[len(versions)-1].String()
	selected, err = module.EscapeVersion(selected)
	if err != nil {
		return "", fmt.Errorf("escaping version: %w", err)
	}

	pluginName := "plugin"
	if runtime.GOOS == "windows" {
		pluginName = "plugin.exe"
	}

	return filepath.Join(pluginDir, selected, pluginName), nil
}

func isLocalPathBinary(addr string) (string, bool) {
	// if the path exists, we use that
	_, err := os.Stat(addr)
	if err == nil {
		fullpath, err := filepath.Abs(addr)
		if err == nil {
			return fullpath, true
		}
	}

	// if its a plain name (doesn't look like an OCI reference) and is prefixed with `fleeting-plugin-`
	// we check to see if the binary exists within $PATH.
	if !strings.ContainsAny(addr, "/:") && strings.HasPrefix(addr, "fleeting-plugin-") {
		p, err := exec.LookPath(addr)
		if err == nil {
			return p, true
		}
	}

	return "", false
}
