package installer

type Option func(*options) error

type options struct {
	dir string
}

func WithDirectory(dir string) Option {
	return func(o *options) error {
		o.dir = dir
		return nil
	}
}
