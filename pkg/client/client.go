package client

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/containerd/platforms"
	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/name"
	v1 "github.com/google/go-containerregistry/pkg/v1"
	"github.com/google/go-containerregistry/pkg/v1/empty"
	"github.com/google/go-containerregistry/pkg/v1/mutate"
	"github.com/google/go-containerregistry/pkg/v1/remote"
	"github.com/google/go-containerregistry/pkg/v1/remote/transport"
	"github.com/google/go-containerregistry/pkg/v1/stream"
	"github.com/google/go-containerregistry/pkg/v1/types"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/internal/defname"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/version"
	"golang.org/x/mod/module"
)

const (
	FleetingArtifactLayerAnnotation = "com.gitlab.fleeting.layer"
	FleetingPluginPkgVersionKey     = "FLEETING_PLUGIN_PKG_VERSION"
)

type Artifact struct {
	ReaderFn func() (io.ReadCloser, error)
	Platform v1.Platform
	Base     string
}

type TagAlias struct {
	Alias   string
	Version version.Version
}

type Client struct {
	remoteOpts []remote.Option
}

// New returns a new client.
func New(opts ...Option) (*Client, error) {
	var options options

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return nil, err
		}
	}

	remoteOpts := []remote.Option{
		remote.WithAuthFromKeychain(authn.DefaultKeychain),
		remote.WithJobs(4),
	}

	return &Client{remoteOpts: remoteOpts}, nil
}

func (c *Client) withRemoteOpts(ctx context.Context, opts ...remote.Option) []remote.Option {
	opts = append(c.remoteOpts, opts...)

	return append(opts, remote.WithContext(ctx))
}

// List returns versions from the remote repository that match the provided
// constraint.
func (c *Client) List(ctx context.Context, addr string) ([]version.Version, error) {
	ref, err := defname.ParseReference(addr, name.WithDefaultTag(""))
	if err != nil {
		return nil, fmt.Errorf("parsing reference %q: %w", addr, err)
	}

	tags, err := remote.List(ref.Context(), c.withRemoteOpts(ctx)...)
	if err != nil {
		return nil, fmt.Errorf("listing tags: %w", err)
	}

	constraint, err := version.NewConstraint(ref.Identifier())
	if err != nil {
		return nil, fmt.Errorf("parsing constraint: %w", err)
	}

	var versions []version.Version
	for _, tag := range tags {
		v, err := version.New(tag)
		if err != nil {
			// ignore invalid versions and aliases
			continue
		}

		versions = append(versions, v)
	}

	return constraint.Match(versions), nil
}

// image is like 'remote.Image()' but uses
// github.com/containerd/platforms for better platform negotiation.
func (c *Client) image(ctx context.Context, ref name.Reference, platform *v1.Platform) (v1.Image, error) {
	idx, err := remote.Index(ref, c.withRemoteOpts(ctx)...)
	if err != nil {
		return nil, fmt.Errorf("fetching index: %w", err)
	}

	indexManifest, err := idx.IndexManifest()
	if err != nil {
		return nil, fmt.Errorf("getting index manifest: %w", err)
	}

	var matcher platforms.MatchComparer
	if platform == nil {
		matcher = platforms.Only(platforms.DefaultSpec())
	} else {
		matcher = platforms.Only(platforms.Platform{
			Architecture: platform.Architecture,
			OS:           platform.OS,
			OSVersion:    platform.OSVersion,
			OSFeatures:   platform.OSFeatures,
			Variant:      platform.Variant,
		})
	}

	sort.SliceStable(indexManifest.Manifests, func(i, j int) bool {
		a := indexManifest.Manifests[i]
		b := indexManifest.Manifests[j]

		return matcher.Less(platforms.Platform{
			Architecture: a.Platform.Architecture,
			OS:           a.Platform.OS,
			OSVersion:    a.Platform.OSVersion,
			OSFeatures:   a.Platform.OSFeatures,
			Variant:      a.Platform.Variant,
		}, platforms.Platform{
			Architecture: b.Platform.Architecture,
			OS:           b.Platform.OS,
			OSVersion:    b.Platform.OSVersion,
			OSFeatures:   b.Platform.OSFeatures,
			Variant:      b.Platform.Variant,
		})
	})

	for _, manifest := range indexManifest.Manifests {
		platform := platforms.Platform{
			Architecture: manifest.Platform.Architecture,
			OS:           manifest.Platform.OS,
			OSVersion:    manifest.Platform.OSVersion,
			OSFeatures:   manifest.Platform.OSFeatures,
			Variant:      manifest.Platform.Variant,
		}

		if matcher.Match(platform) {
			image, err := idx.Image(manifest.Digest)
			if err != nil {
				log.Fatalf("fetching image for manifest %v: %v", manifest.Digest, err)
			}

			return image, nil
		}
	}

	return nil, fmt.Errorf("didn't find an image matching platform")
}

// Push pushes a new artifact and updates version aliases as necessary.
func (c *Client) Push(ctx context.Context, addr string, artifacts []Artifact) (string, error) {
	dest, err := defname.ParseReference(addr, name.WithDefaultTag(""))
	if err != nil {
		return "", fmt.Errorf("parsing reference %q: %w", addr, err)
	}

	if _, err := version.New(dest.Identifier()); err != nil {
		return "", fmt.Errorf("parsing version: %w", err)
	}

	index := v1.ImageIndex(empty.Index)
	for idx := range artifacts {
		artifact := artifacts[idx]

		src := empty.Image
		if artifact.Base != "" {
			base, err := defname.ParseReference(artifact.Base)
			if err != nil {
				return "", fmt.Errorf("parsing reference: %v", err)
			}

			src, err = c.image(ctx, base, &artifact.Platform)
			if err != nil {
				return "", fmt.Errorf("getting base image: %w", err)
			}
		}

		cfg, err := src.ConfigFile()
		if err != nil {
			return "", fmt.Errorf("getting config file: %w", err)
		}

		if artifact.Platform.OS == "windows" {
			cfg.Config.Entrypoint = []string{"/plugin/plugin.exe"}
		} else {
			cfg.Config.Entrypoint = []string{"/plugin/plugin"}
		}
		cfg.Config.Env = append(cfg.Config.Env, FleetingPluginPkgVersionKey+"="+dest.Identifier())

		image := mutate.MediaType(src, types.OCIManifestSchema1)
		image, err = mutate.Config(image, cfg.Config)
		if err != nil {
			return "", fmt.Errorf("updating config: %w", err)
		}
		image = mutate.ConfigMediaType(image, types.OCIConfigJSON)

		rc, err := artifact.ReaderFn()
		if err != nil {
			return "", fmt.Errorf("getting reader for artifact %v: %w", artifact.Platform, err)
		}

		image, err = mutate.Append(image,
			mutate.Addendum{
				Layer: stream.NewLayer(rc, stream.WithCompressionLevel(gzip.DefaultCompression)),
				Annotations: map[string]string{
					FleetingArtifactLayerAnnotation: dest.Identifier(),
				},
			},
		)
		if err != nil {
			return "", fmt.Errorf("appending content failed: %w", err)
		}

		annotations := map[string]string{
			"org.opencontainers.image.version": dest.Identifier(),
			"org.opencontainers.image.created": time.Now().UTC().Format(time.RFC3339),
		}

		baseName := artifact.Base
		if artifact.Base == "" {
			baseName = "scratch"
		}
		annotations["org.opencontainers.image.base.name"] = baseName

		index = mutate.AppendManifests(index, mutate.IndexAddendum{
			Add: image,
			Descriptor: v1.Descriptor{
				Platform:    &artifact.Platform,
				Annotations: annotations,
			},
		})
	}

	if err := remote.WriteIndex(dest, index, c.withRemoteOpts(ctx)...); err != nil {
		return "", fmt.Errorf("writing index: %w", err)
	}

	if _, err := c.UpdateAliases(ctx, addr, false); err != nil {
		return "", fmt.Errorf("updating aliases: %w", err)
	}

	h, err := index.Digest()
	if err != nil {
		return "", fmt.Errorf("getting digest: %w", err)
	}

	return dest.Context().Digest(h.String()).String(), nil
}

// UpdateAliases updates any out-of-sync version aliases.
func (c *Client) UpdateAliases(ctx context.Context, addr string, dryrun bool) ([]TagAlias, error) {
	ref, err := defname.ParseReference(addr, name.WithDefaultTag(""))
	if err != nil {
		return nil, fmt.Errorf("parsing reference %q: %w", addr, err)
	}

	tags, err := remote.List(ref.Context(), c.withRemoteOpts(ctx)...)
	if err != nil {
		return nil, fmt.Errorf("listing tags: %w", err)
	}

	constraint, err := version.NewConstraint(ref.Identifier())
	if err != nil {
		return nil, fmt.Errorf("parsing constraint: %w", err)
	}

	type aliasInfo struct {
		Version version.Version
		Digest  *v1.Hash
	}

	aliases := map[string]aliasInfo{"latest": {}}

	var versions []version.Version
	for _, tag := range tags {
		v, err := version.New(tag)
		if err != nil {
			// ignore invalid versions
			continue
		}

		versions = append(versions, v)

		majorAlias := strconv.FormatInt(v.Major, 10)
		minorAlias := strconv.FormatInt(v.Major, 10) + "." + strconv.FormatInt(v.Minor, 10)

		if v.GreaterThan(aliases["latest"].Version) {
			aliases["latest"] = aliasInfo{Version: v}
		}

		if v.GreaterThan(aliases[majorAlias].Version) {
			aliases[majorAlias] = aliasInfo{Version: v}
		}

		if v.GreaterThan(aliases[minorAlias].Version) {
			aliases[minorAlias] = aliasInfo{Version: v}
		}
	}

	var results []TagAlias
	for _, v := range constraint.Match(versions) {
		versionDesc, err := remote.Head(ref.Context().Tag(v.String()), c.withRemoteOpts(ctx)...)
		if err != nil {
			return nil, fmt.Errorf("getting descriptor head for version %v: %w", v, err)
		}

		for alias, info := range aliases {
			if !v.Equal(info.Version) {
				continue
			}

			if info.Digest == nil {
				desc, err := remote.Head(ref.Context().Tag(alias), c.withRemoteOpts(ctx)...)
				if is404(err) {
					desc = &v1.Descriptor{}
					err = nil
				}
				if err != nil {
					return nil, fmt.Errorf("getting descriptor head for alias %v: %w", alias, err)
				}

				info.Digest = &desc.Digest
			}

			if versionDesc.Digest == *info.Digest {
				continue
			}

			results = append(results, TagAlias{
				Alias:   alias,
				Version: v,
			})
		}
	}

	var errs []error
	if !dryrun {
		descriptorCache := make(map[string]*remote.Descriptor)
		for _, result := range results {
			key := result.Version.String()

			if _, ok := descriptorCache[key]; ok {
				continue
			}

			desc, err := remote.Get(ref.Context().Tag(key), c.withRemoteOpts(ctx)...)
			if err != nil {
				return nil, fmt.Errorf("getting descriptor for version %v: %w", result.Version, err)
			}

			descriptorCache[key] = desc
		}

		for _, result := range results {
			err := remote.Tag(ref.Context().Tag(result.Alias), descriptorCache[result.Version.String()], c.withRemoteOpts(ctx)...)
			if err != nil {
				errs = append(errs, fmt.Errorf("tagging alias %v=%v: %w", result.Alias, result.Version, err))
			}
		}
	}

	return results, errors.Join(errs...)
}

// Pull finds and extracts the fleeting plugin layer from the repository and
// unpacks it to the provided directory.
func (c *Client) Pull(ctx context.Context, addr, dir string) error {
	os.MkdirAll(filepath.Join(dir, "temp"), 0o777)

	ref, err := defname.ParseReference(addr)
	if err != nil {
		return fmt.Errorf("parsing reference: %v", err)
	}

	image, err := c.image(ctx, ref, nil)
	if err != nil {
		return fmt.Errorf("getting image: %w", err)
	}

	manifest, err := image.Manifest()
	if err != nil {
		return fmt.Errorf("getting image manifest: %w", err)
	}

	layers, err := image.Layers()
	if err != nil {
		return fmt.Errorf("getting image layers: %w", err)
	}

	var fleetingLayer *v1.Descriptor
	var fleetingLayerIdx int
	var ver string

	for idx, layer := range manifest.Layers {
		v, ok := layer.Annotations[FleetingArtifactLayerAnnotation]
		if !ok {
			continue
		}

		fleetingLayer = &manifest.Layers[idx]
		fleetingLayerIdx = idx
		ver = v
		break
	}

	if fleetingLayer == nil {
		return fmt.Errorf("fleeting artifact layer not found")
	}

	if _, err := version.New(ver); err != nil {
		return fmt.Errorf("fleeting artifact layer version is invalid: %w", err)
	}

	pluginDir, pluginVersion, err := pluginPath(ref.Context().Name(), ver)
	if err != nil {
		return err
	}
	pluginDir = filepath.Join(dir, pluginDir, pluginVersion)

	// check if already exists
	if _, err := os.Stat(pluginDir); err == nil {
		return nil
	}

	rc, err := layers[fleetingLayerIdx].Uncompressed()
	if err != nil {
		return fmt.Errorf("opening uncompressed reader %v: %w", fleetingLayer.Digest, err)
	}
	defer rc.Close()

	tmpDir, err := os.MkdirTemp(dir, "")
	if err != nil {
		return fmt.Errorf("creating temporary directory for plugin: %w", err)
	}
	defer os.RemoveAll(tmpDir)

	tr := tar.NewReader(rc)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("tar next %v: %w", fleetingLayer.Digest, err)
		}

		if hdr.Typeflag != tar.TypeReg {
			continue
		}

		// Windows containers require that files are placed within "Files/"
		// and we attempt to remove this regardless of the current OS just
		// so that it works universally.
		hdr.Name = strings.TrimPrefix(hdr.Name, "Files/")

		if !strings.HasPrefix(hdr.Name, "plugin") {
			return fmt.Errorf("file in archive was not in 'plugin' dir")
		}

		hdr.Name = strings.TrimPrefix(hdr.Name, "plugin/")

		f, err := os.OpenFile(filepath.Join(tmpDir, hdr.Name), os.O_CREATE|os.O_RDWR, hdr.FileInfo().Mode())
		if err != nil {
			return fmt.Errorf("creating file: %w", err)
		}

		if _, err := io.Copy(f, tr); err != nil {
			return fmt.Errorf("copying file: %w", err)
		}

		if err := f.Close(); err != nil {
			return fmt.Errorf("closing file: %w", err)
		}
	}

	if err := os.MkdirAll(filepath.Dir(pluginDir), 0o777); err != nil {
		return fmt.Errorf("creating plugin directory: %w", err)
	}

	return os.Rename(tmpDir, pluginDir)
}

func is404(err error) bool {
	var terr *transport.Error
	if errors.As(err, &terr) {
		return terr.StatusCode == http.StatusNotFound
	}

	return false
}

func pluginPath(dir, ver string) (string, string, error) {
	pluginDir, err := module.EscapePath(dir)
	if err != nil {
		return "", "", fmt.Errorf("escaping plugin path: %w", err)
	}

	pluginVersion, err := module.EscapeVersion(ver)
	if err != nil {
		return "", "", fmt.Errorf("escaping plugin version: %w", err)
	}

	return pluginDir, pluginVersion, nil
}
