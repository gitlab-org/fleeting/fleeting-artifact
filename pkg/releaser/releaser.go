package releaser

import (
	"archive/tar"
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log/slog"
	"os"
	"path"
	"path/filepath"
	"strings"

	v1 "github.com/google/go-containerregistry/pkg/v1"
	"gitlab.com/gitlab-org/fleeting/fleeting-artifact/pkg/client"
)

type packageConfig struct {
	OS        string
	OSVersion string
	Arch      string
	Variant   string
	Base      string
	Dir       string
}

func (pkg packageConfig) String() string {
	var sb strings.Builder

	sb.WriteString(pkg.OS)

	if pkg.Arch != "" {
		sb.WriteByte('/')
		sb.WriteString(pkg.Arch)
	}

	if pkg.Variant != "" {
		sb.WriteByte('/')
		sb.WriteString(pkg.Variant)
	}

	if pkg.OSVersion != "" {
		sb.WriteByte(':')
		sb.WriteString(pkg.OSVersion)
	}

	return sb.String()
}

func Release(ctx context.Context, addr string, opts ...Option) (string, error) {
	var options options

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return "", err
		}
	}

	if options.dir == "" {
		options.dir = "dist"
	}

	if options.logger == nil {
		options.logger = slog.Default()
	}

	c, err := client.New()
	if err != nil {
		return "", fmt.Errorf("creating client: %w", err)
	}

	packages := []packageConfig{
		{OS: "linux", Arch: "386", Dir: "linux/386"},
		{OS: "linux", Arch: "amd64", Dir: "linux/amd64"},
		{OS: "linux", Arch: "arm", Variant: "v5", Dir: "linux/armv5"},
		{OS: "linux", Arch: "arm", Variant: "v6", Dir: "linux/armv6"},
		{OS: "linux", Arch: "arm", Variant: "v7", Dir: "linux/armv7"},
		{OS: "linux", Arch: "arm64", Dir: "linux/arm64"},
		{OS: "linux", Arch: "mips64le", Dir: "linux/mips64le"},
		{OS: "linux", Arch: "ppc64le", Dir: "linux/ppc64le"},
		{OS: "linux", Arch: "riscv64", Dir: "linux/riscv64"},
		{OS: "linux", Arch: "s390x", Dir: "linux/s390x"},
		{OS: "freebsd", Arch: "386", Dir: "freebsd/386"},
		{OS: "freebsd", Arch: "amd64", Dir: "freebsd/amd64"},
		{OS: "freebsd", Arch: "arm", Variant: "v5", Dir: "freebsd/armv5"},
		{OS: "freebsd", Arch: "arm", Variant: "v6", Dir: "freebsd/armv6"},
		{OS: "freebsd", Arch: "arm", Variant: "v7", Dir: "freebsd/armv7"},
		{OS: "freebsd", Arch: "arm64", Dir: "freebsd/arm64"},
		{OS: "darwin", Arch: "amd64", Dir: "darwin/amd64"},
		{OS: "darwin", Arch: "arm64", Dir: "darwin/arm64"},
		{OS: "windows", Arch: "386", Dir: "windows/386"},
		{OS: "windows", Arch: "amd64", OSVersion: "10.0.20348", Dir: "windows/amd64", Base: "mcr.microsoft.com/windows/nanoserver:ltsc2022"},
		{OS: "windows", Arch: "amd64", OSVersion: "10.0.17763", Dir: "windows/amd64", Base: "mcr.microsoft.com/windows/nanoserver:ltsc2019"},
		{OS: "zos", Arch: "s390x", Dir: "zos/s390x"},
	}

	var artifacts []client.Artifact

	for _, pkg := range packages {
		dir := filepath.Join(options.dir, pkg.Dir)

		_, err := os.Stat(dir)
		found := err == nil
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return "", fmt.Errorf("reading directory: %w", err)
		}

		options.logger.Info("packaging", "platform", &pkg, "dir", dir, "skipping", !found)
		if !found {
			continue
		}

		artifacts = append(artifacts, client.Artifact{
			ReaderFn: archiveReader(pkg.OS, os.DirFS(dir)),
			Platform: v1.Platform{Architecture: pkg.Arch, OS: pkg.OS, OSVersion: pkg.OSVersion, Variant: pkg.Variant},
			Base:     pkg.Base,
		})
	}

	return c.Push(ctx, addr, artifacts)
}

func archiveReader(osName string, fsys fs.FS) func() (io.ReadCloser, error) {
	expectedPluginName := "plugin"
	if osName == "windows" {
		expectedPluginName = "plugin.exe"
	}

	return func() (rc io.ReadCloser, err error) {
		tmp, err := os.CreateTemp("", "")
		if err != nil {
			return nil, fmt.Errorf("creating temporary directory: %w", err)
		}

		defer func() {
			if err != nil {
				tmp.Close()
				os.Remove(tmp.Name())
			}
		}()

		tw := tar.NewWriter(tmp)
		defer tw.Close()

		if osName == "windows" {
			// windows requires Files/Hives directories.
			for _, dir := range []string{"Files", "Hives"} {
				if err := tw.WriteHeader(&tar.Header{
					Typeflag: tar.TypeDir,
					Name:     dir,
					Mode:     0555,
					Format:   tar.FormatPAX,
				}); err != nil {
					return nil, fmt.Errorf("writing windows dir %q: %w", dir, err)
				}
			}
		}

		foundPlugin := false
		err = fs.WalkDir(fsys, ".", func(name string, d fs.DirEntry, err error) error {
			if !foundPlugin {
				foundPlugin = name == expectedPluginName

				// check executable bit
				if foundPlugin {
					info, err := d.Info()
					if err != nil {
						return fmt.Errorf("plugin binary stat: %w", err)
					}
					if info.Mode().Perm()&0o111 == 0 {
						return fmt.Errorf("plugin binary needs to be executable")
					}
				}
			}

			if err != nil {
				return err
			}

			info, err := d.Info()
			if err != nil {
				return err
			}

			if !d.IsDir() && !info.Mode().IsRegular() {
				return errors.New("cannot add non-regular file")
			}

			h, err := tar.FileInfoHeader(info, "")
			if err != nil {
				return err
			}

			if osName == "windows" {
				h.Name = path.Join("Files", "plugin", name)

				if info.Mode().Perm()&0o111 != 0 {
					// windows magic for making an executable file
					h.Format = tar.FormatPAX
					if h.PAXRecords == nil {
						h.PAXRecords = map[string]string{}
					}
					h.PAXRecords["MSWINDOWS.rawsd"] = "AQAAgBQAAAAkAAAAAAAAAAAAAAABAgAAAAAABSAAAAAhAgAAAQIAAAAAAAUgAAAAIQIAAA=="
				}
			} else {
				h.Name = path.Join("plugin", name)
			}
			if err := tw.WriteHeader(h); err != nil {
				return err
			}

			if !d.IsDir() {
				f, err := fsys.Open(name)
				if err != nil {
					return err
				}
				defer f.Close()

				if _, err := io.Copy(tw, f); err != nil {
					return err
				}
			}

			return nil
		})
		if err != nil {
			return nil, err
		}

		if !foundPlugin {
			return nil, fmt.Errorf("no plugin binary %q found", expectedPluginName)
		}

		if err := tw.Close(); err != nil {
			return nil, err
		}

		if err := tmp.Sync(); err != nil {
			return nil, err
		}

		if _, err := tmp.Seek(0, io.SeekStart); err != nil {
			return nil, err
		}

		return &readCloseRemover{tmp}, nil
	}
}

type readCloseRemover struct {
	*os.File
}

func (rcr *readCloseRemover) Close() error {
	defer os.Remove(rcr.Name())

	return rcr.File.Close()
}
